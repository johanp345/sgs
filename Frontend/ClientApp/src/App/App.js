import React, { useState,useEffect } from 'react';
import { Switch, Route,Redirect,useHistory } from 'react-router-dom';

import Swal from 'sweetalert2';

import {CheckAccessToken} from '../components/Utils';
import { Layout } from '../components/Layout';
import Login from '../pages/Login';
import ForgotPass from '../pages/ForgotPass';
import Home from '../pages/Home';
import Afiliaciones from '../pages/Afiliaciones';
import User from '../pages/User';
import _404 from '../pages/404';
import "bootstrap";
import "../assets/icons/styles.css";
import "../assets/css/main.scss";
import "../assets/css/theme.scss";
import "../assets/css/responsive.scss";

export default (props)=> {
  //const login = true || cookies.get('username');
  const {getGlobalConfig,global,router} = props;
  const history=useHistory();
  useEffect(()=>{
    getGlobalConfig();
    
  },[])
  
  useEffect(()=>{
    //console.log(router);
    if(global.refreshTokenTimeout && sessionStorage.getItem('usuario-token')){
      console.log(CheckAccessToken(sessionStorage.getItem('usuario-token'), global));
      switch (CheckAccessToken(sessionStorage.getItem('usuario-token'), global)) {
        case "cerrar":
          sessionStorage.clear();
          history.replace("(/login/")
        break;
        case "actualizar":
          console.log("act");
        break;
      }
    }
  },[router])
  
  useEffect(()=>{
    if(global.refreshTokenTimeout && sessionStorage.getItem('usuario-token')){
      //console.log(CheckAccessToken(sessionStorage.getItem('usuario-token'), global));
      switch (CheckAccessToken(sessionStorage.getItem('usuario-token'), global)) {
        case "cerrar":
          sessionStorage.clear();
          history.replace("(/login/")
        break;
        case "actualizar":
          console.log("act");
        break;
      }
    }
  },[global])

    return (
      <>
        {
          (!global.login && !sessionStorage.getItem('usuario-token'))?
              <Layout>
                <Switch>
                    <Route  exact path='/login/' component={Login} />
                    <Route  exact path='/forgotpass/' component={ForgotPass} />
                    <Route  path='' render ={()=><Redirect to="/login/"/>} />
                </Switch>
              </Layout>
            :
              <Switch>
                <Route  path='/login/' render ={()=><Redirect to="/"/>}/>
                <Route exact path='/'>
                  <Layout>
                    <Home/>
                  </Layout>
                </Route>
                <Route path='/afiliaciones/:accion?'>
                  <Layout layout="menu">
                    <Afiliaciones/>
                  </Layout>
                </Route>
                <Route path='/user'>
                  <Layout layout="menu">
                    <User/>
                  </Layout>
                </Route>
                <Route  path=''>
                  <Layout>
                    <_404/>
                  </Layout>
                </Route>
              </Switch>
        }
      </>
    );
}
