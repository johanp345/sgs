import axios from 'axios';

const axiosInstanceAPI = axios.create({
    headers: { 'content-type': 'application/json' }
});

const Services = {
    global:{
        getConfigGen:()=>
            axiosInstanceAPI.get(`/configClientApp.json`).catch((error)=>error.response),
    },
    access:{
        login: (url,data)=> axiosInstanceAPI.post(url,data).catch((error)=>error.response),
    },
    afiliaciones:{
        getAfiliaciones: (url,token)=> axios.get(url,{
            headers: {
                'content-type': 'application/json',
                'Authorization': 'bearer ' + token
            }
        }).catch((error)=>error.response),
    },
}

export default Services;