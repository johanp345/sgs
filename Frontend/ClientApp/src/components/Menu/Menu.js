import React, { useState,useEffect } from 'react';
import { withTranslation } from 'react-i18next';
import {Link} from 'react-router-dom';
import logo from '../../assets/images/logo.svg';
//const cookies = new Cookies();
const Home = (props)=> {
  const {global,t,menu} = props;
  const [Loading, setLoading] = useState(false)
  useEffect(()=>{
    //console.log(menu);
  },[menu])
  
    return (
      <section id="menuL" className="">
        <figure>
          <img src={logo}/>
        </figure>
        <Link to="/" className='itemMenu'>
          <p>
            <i className="icon-menu"></i>
            <span>{t("menu")}</span>
          </p>
        </Link>
        {
          menu.items.map(item=>{
            return <Link key={item.url} to={item.url} className='itemMenu'>
                      <p>
                        <i className={item.icono}></i>
                        <span>{t(item.nombre)}</span>
                      </p>
                    </Link>
          })
        }
      </section>
    );
}

export default withTranslation()(Home)