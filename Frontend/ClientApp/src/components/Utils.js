import moment from "moment";
import jwt_decode from 'jwt-decode';


export const validarForm=(form)=>{
    let valid = true;
    let _F = {...form};
    for (let key of Object.keys(_F)){
        if(_F[key].value.trim()==="" && _F[key].required){
            _F[key].error=true;
            valid=false;
        }
    }
    return {valid,_F}
}

export const horarioValido =(terminal,timeZone)=>{
    var now = moment().utcOffset(timeZone);
    var dow = now.day();
    var horaValido = true;

    if (dow === moment().day("Saturday")) {
      if (terminal.HorarioInicioSabado !== null) {
        if (moment(terminal.HorarioInicioSabado, "HH:mm:ss").isAfter(now)) {
          horaValido = false;
        }
      }

      if (terminal.HorarioFinSabado !== null) {
        if (moment(terminal.HorarioFinSabado, "HH:mm:ss").isBefore(now)) {
          horaValido = false;
        }
      }
    }
    else if (dow === moment().day("Sunday")) {
      if (terminal.HorarioInicioDomingo !== null) {
        if (moment(terminal.HorarioInicioDomingo, "HH:mm:ss").isAfter(now)) {
          horaValido = false;
        }
      }

      if (terminal.HorarioFinDomingo !== null) {
        if (moment(terminal.HorarioFinDomingo, "HH:mm:ss").isBefore(now)) {
          horaValido = false;
        }
      }
    }
    else if (terminal.HorarioInicioSemana !== null ||
      terminal.HorarioFinSemana !== null) {
      if (terminal.HorarioInicioSemana !== null &&
        moment(terminal.HorarioInicioSemana, "HH:mm:ss").isAfter(now)) {
        horaValido = false;
      }

      if (terminal.HorarioFinSemana !== null &&
        moment(terminal.HorarioFinSemana, "HH:mm:ss").isBefore(now)) {
        horaValido = false;
      }
    }
    return horaValido;
}

export const CheckAccessToken= (token, config) =>{
  const decoded = jwt_decode(token);
  const expLocal = new Date(decoded.exp * 1000);
  const exp = new Date(
    expLocal.getTime() + (expLocal.getTimezoneOffset() * 60000));
  const now = new Date();
  let nowUtc = new Date(now.getTime() + (now.getTimezoneOffset() * 60000));
  const expTimestamp = exp.getTime();
  const nowUtcTimestamp = nowUtc.getTime();
  if (nowUtcTimestamp >= expTimestamp) {
    const diff = ((nowUtcTimestamp - expTimestamp) / 1000) / 60;
    if (diff >= config.refreshTokenTimeout) {
      return "cerrar";
    }
    else {
      return "actualizar";
      // $.ajax({
      //   url: `${config.apiUrl}/operadorterminal/refresh.token`,
      //   method: 'POST',
      //   headers: {
      //     'refresh-token': refreshToken
      //   },
      //   cache: false,
      //   success: (data) => {
      //     sessionStorage.setItem('usuario-token', data.Token);
      //     that.setState({
      //       token: data.Token
      //     });
      //     if (typeof callback === 'function') {
      //       callback();
      //     }
      //   },
      //   error: (xhr, status, err) => {
      //     console.log('response', xhr.responseText);
      //     console.log('status', status);
      //     console.log('err', err);
      //     swal({
      //       text: 'Ocurrió un error renovando la sesión',
      //       buttons: {
      //         confirm: 'Aceptar',
      //       },
      //       icon: 'warning'
      //     }).then(v => {
      //       if (v) {
      //         sessionStorage.clear();
      //         window.location = '/';
      //       }
      //     });
      //   }
      // });
    }
  }else{
    return "continuar";
  }
}