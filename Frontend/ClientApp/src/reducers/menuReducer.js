import {
  GET_MENU
} from '../actions/menu/types';

const initialState = {
  items:[]
};

const menuReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_MENU: {
      return {
        ...state,
        items: action.payload,
      };
    }
    default:
      return state;
  }
};

export default menuReducer;
