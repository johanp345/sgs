import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import globalReducer from './globalReducer';
import menuReducer from './menuReducer';

const rootReducer = (history) =>
  combineReducers({
    router: connectRouter(history),
    global: globalReducer,
    menu: menuReducer,
  });

export default rootReducer;
