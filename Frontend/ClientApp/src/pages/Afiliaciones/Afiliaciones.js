import React, { useState,useEffect } from 'react';
import { withTranslation } from 'react-i18next';

import {Link,useParams} from 'react-router-dom';
import Spinner from '../../components/Spinner';
import Api from '../../services/services';
import {itemsAfiliaciones} from '../../components/menuItems';

//const cookies = new Cookies();
const Afiliaciones = (props)=> {
  const {global,t,getMenu} = props;
  const {accion} = useParams();
  const [Usuarios,setUsuarios]=useState([]);

  const [Loading, setLoading] = useState(false)

  useEffect(()=>{
    getMenu(itemsAfiliaciones);
  },[])

  useEffect(()=>{
    if(global.apiUrl){
      (async ()=>{
        const {status,data} = await Api.afiliaciones.getAfiliaciones(global.apiUrl+"/pagador",sessionStorage.getItem('usuario-token'))
        console.log(status,data);
        if(status===200){
          setUsuarios(data)
        }
      })()
    }
  },[global])

  const submit=(e)=>{
    e.preventDefault();
    
  }
    return (
      <section id="pageAfiliaciones" className="h-100">
        {
          (accion)?
          null
          :
          <>
            <header className="headerInterno d-flex w-100 justify-content-between">
              <h2 className="cTexto1">{t('tituloAfiliaciones')}</h2>
              <i className="icon-notificacion placeHolder"></i>
            </header>
            <section className="wrapContent">
              <div className="d-flex flex-wrap w-100 align-items-end">
                <div className="resumen d-flex justify-content-between align-items-center">
                  <div>
                    <span className="cTexto4">RESUMEN DE AFILIACIONES</span>
                    <span className="cTexto2">Saldo promedio: $8.25</span>
                  </div>
                  <p className="cTexto2">376</p>
                </div>

                <div className="wrapSearch ml-auto">
                  <form onSubmit={submit}>
                    <input type="text" name="buscar" disabled={Loading}/>
                    <i className="icon-lupa cTextoI"></i>
                  </form>
                </div>
                <Link to="afiliaciones/addUsuario" className="btn-add p-0 btn bg-ppal bg-btn-gen d-flex align-items-center">
                  <span className="d-flex align-items-center" >
                    <i className="icon-add-usuario mr-2"></i>
                    <p className="mb-0">Agregar usuario</p>
                  </span>
                </Link>
              </div>

              <section className="listaUsuarios">
                <table className="tableGen" id="tablaAfil">
                  <thead>
                    <tr>
                      <th>Usuario</th>
                      <th>Teléfono</th>
                      <th>Fecha de afiliación</th>
                      <th>status</th>
                      <th>saldo</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      Usuarios.map(u=>{
                        return <tr key={u.Id}>
                                  <td>
                                    <p>{u.Nombre} {u.Apellido}</p>
                                    <span>{u.Email}</span>
                                  </td>  
                                  <td>{u.Telefono}</td>
                                  <td>fecha af.</td>
                                  <td>{u.Estatus}</td>
                                  <td>saldo</td>
                                  <td><i className="icon-puntos"></i></td>
                                </tr>
                      })
                    }
                  </tbody>
                </table>
              </section>
            </section>
          </>
        }
      </section>
    );
}

export default withTranslation()(Afiliaciones)