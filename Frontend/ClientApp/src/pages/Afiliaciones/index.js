import { connect } from "react-redux";
import Afiliaciones from "./Afiliaciones";

import { getGlobalConfig} from "../../actions/global/creators";
import { getMenu} from "../../actions/menu/creators";

const mapStateToProps = (state) => ({
  global: state.global,
  menu:state.menu
});

const mapDispatchToProps = (dispatch) => ({
  getGlobalConfig: (domain) => dispatch(getGlobalConfig(domain)),
  getMenu: (menu) => dispatch(getMenu(menu)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Afiliaciones);
