import React, { useState,useEffect } from 'react';
import {Link} from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import logoCli from '../../assets/images/logo-cli.svg';

//const cookies = new Cookies();
const ForgotPass = (props)=> {
  const {global,t} = props;
  const [Form,setForm]=useState({
    correo:"",
  })
  useEffect(()=>{
    console.log(props);
  })
  const submit=(e)=>{
    e.preventDefault();
    console.log("submit");
    console.log(Form);
  }
  const handleForm=(e)=>{
    let {name,value}=e.target;
    setForm({
      ...Form,
      [name]:value
    })
  }
    return (
      <div className="d-flex align-items-stretch w-100 h-100" id="pageLogin">
        <section className="w-100 bg-ppal">

        </section>
        <section className="w-100 d-flex align-items-center justify-content-center position-relative">
          <Link to="/login"  className="f14 cTexto1" style={{position:"absolute",top:"20px",left:"20px"}}>{t('volver')}</Link>
          <section className="wrapLogin ">
            <figure>
              <img src={logoCli}/>
            </figure>
            <h2 className="cTexto1 f20">{t('tituloForgotPass')}</h2>
            <form onSubmit={submit} autoComplete="new-password">
              <div className="row">
                <div className="col-12">
                  <div className="wrapInput">
                    <input onChange={handleForm} type="text" value={Form.correo} name="correo" autoComplete="new-password"/>
                    <label className="placeHolder">{t('formCorreo')}</label>
                  </div>
                </div>
                <div className="col-12">
                  <button className="btn btn-block btnGeneral"><span>{t('buttonEnviar')}</span></button>
                </div>
              </div>
            </form>
          </section>
        </section>
      </div>
    );
}

export default withTranslation()(ForgotPass)