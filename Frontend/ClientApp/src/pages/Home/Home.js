import React, { useState,useEffect } from 'react';
import { withTranslation } from 'react-i18next';
import {Link} from 'react-router-dom';
import Spinner from '../../components/Spinner';
import logo from '../../assets/images/logo.svg';
//const cookies = new Cookies();
const Home = (props)=> {
  const {global,t} = props;
  const [Loading, setLoading] = useState(false)
  useEffect(()=>{
    //console.log(props);
    
  },[])
  const submit=(e)=>{
    e.preventDefault();
    if(!Loading){
      setLoading(true)
      setTimeout(() => {
        setLoading(false)
      }, 30000);
    }
  }
    return (
      <section id="pageHome" className="h-100">
        <header className="bg-ppal align-items-center d-flex">
          <div className="container d-flex w-100 justify-content-between align-items-center">
              <figure>
                <img src={logo}/>
              </figure>
              <form onSubmit={submit}>
                <input type="text" name="buscar" disabled={Loading}/>
                {
                Loading?
                <Spinner/>
                :
                <i className="icon-lupa cTextoI"></i>
                }
              </form>
              <i className="icon-notificacion cTextoI"></i>
          </div>
        </header>
        <section className="wrap-opciones container">
          <div className="row">
            <div className="col-12 col-md-6 col-lg-4">
              <Link to="/afiliaciones" className="opcionHome d-flex flex-wrap w-100 align-items-center justify-content-center">
                  <i className="icon-afiliaciones cTexto placeHolder"></i>
                  <p className=" align-self-end w-100">
                    <span className="d-block cTexto1">Afiliciones / Suscripciones</span>
                    <span className="d-block placeHolder">Aenean massa um sociis natoque pena</span>
                  </p>
              </Link>
            </div>
            <div className="col-12 col-md-6 col-lg-4">
              <Link to="" className="opcionHome d-flex flex-wrap w-100 align-items-center justify-content-center">
                  <i className="icon-saldo cTexto placeHolder"></i>
                  <p className=" align-self-end w-100">
                    <span className="d-block cTexto1">Saldo</span>
                    <span className="d-block placeHolder">Aenean massa um sociis natoque pena</span>
                  </p>
              </Link>
            </div>
            <div className="col-12 col-md-6 col-lg-4">
              <Link to="" className="opcionHome d-flex flex-wrap w-100 align-items-center justify-content-center">
                  <i className="icon-caja cTexto placeHolder"></i>
                  <p className=" align-self-end w-100">
                    <span className="d-block cTexto1">Caja</span>
                    <span className="d-block placeHolder">Aenean massa um sociis natoque pena</span>
                  </p>
              </Link>
            </div>
            <div className="col-12 col-md-6 col-lg-4">
              <Link to="" className="opcionHome d-flex flex-wrap w-100 align-items-center justify-content-center">
                  <i className="icon-reportes cTexto placeHolder"></i>
                  <p className=" align-self-end w-100">
                    <span className="d-block cTexto1">Administracion & Reportes</span>
                    <span className="d-block placeHolder">Aenean massa um sociis natoque pena</span>
                  </p>
              </Link>
            </div>
            <div className="col-12 col-md-6 col-lg-4">
              <Link to="" className="opcionHome d-flex flex-wrap w-100 align-items-center justify-content-center">
                  <i className="icon-soporte cTexto placeHolder"></i>
                  <p className=" align-self-end w-100">
                    <span className="d-block cTexto1">Soporte</span>
                    <span className="d-block placeHolder">Aenean massa um sociis natoque pena</span>
                  </p>
              </Link>
            </div>
            <div className="col-12 col-md-6 col-lg-4">
              <Link to="" className="opcionHome d-flex flex-wrap w-100 align-items-center justify-content-center">
                  <i className="icon-servicios cTexto placeHolder"></i>
                  <p className=" align-self-end w-100">
                    <span className="d-block cTexto1">Pago de servicios</span>
                    <span className="d-block placeHolder">Aenean massa um sociis natoque pena</span>
                  </p>
              </Link>
            </div>
          </div>
        </section>
      </section>
    );
}

export default withTranslation()(Home)