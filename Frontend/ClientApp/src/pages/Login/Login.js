import React, { useState,useEffect } from 'react';
import { withTranslation } from 'react-i18next';
import {Link} from 'react-router-dom';
import logoCli from '../../assets/images/logo-cli.svg';
import Spinner from '../../components/Spinner';
import {horarioValido,validarForm} from '../../components/Utils';
import Api from '../../services/services';
import logo from '../../assets/images/logo.svg';

//const cookies = new Cookies();
const Login = (props)=> {
  const {getGlobalConfig,global,t,history} = props;
  const [Loading,setLoading]=useState(false);
  const [Form,setForm]=useState({
    usuario:{
      value:"",
      required:true,
      error:null
    },
    password:{
      value:"",
      required:true,
      error:null
    }
  })
  const [ErrorLogin,setErrorLogin]=useState(false)
  
  const [AccesosMultiple,setAccesosMultiple]=useState(false)
  const [Terminales,setTerminales]=useState([])
  const [UsuarioToken,setUsuarioToken]=useState(null)
  const [RefreshToken,setRefreshToken]=useState(null)
  const [PrimerLogin,setPrimerLogin]=useState(false)
  const [Operador,setOperador]=useState({})
  useEffect(()=>{
    //console.log(props,global,t);
  },[])
  const submit=(e)=>{
    e.preventDefault();
    let {valid,_F} = validarForm(Form)
    if(!valid){
      setForm(_F);
      return 0;
    }
    setLoading(true);
    (async ()=>{
        const {status,data} = await Api.access.login(global.apiUrl+"/operadorterminal/login",{Login:Form.usuario.value,Password:Form.password.value})
        if(status===200){
          setRefreshToken(data.RefreshToken)
          setUsuarioToken(data.Token)
          setOperador(data.Usuario)
          setPrimerLogin(data.PrimerLogin)
          
          if (data.Accesos.length === 1) {
            handleTerminal(data.Accesos[0])
          }
          else {
            setTerminales(data.Accesos)
            setAccesosMultiple(true)
          }
        }else if(status===401){
          setErrorLogin(true)
        }
        setLoading(false);
      }
      )()
  }
    
  const handleTerminal=(terminal)=>{
    setOperador({...Operador,IdTerminal:terminal.Id})
    
    sessionStorage.setItem('usuario-token', UsuarioToken);
    sessionStorage.setItem('taquilla-config', JSON.stringify(global));
    sessionStorage.setItem('taquilla-usuario', JSON.stringify(Operador));
    sessionStorage.setItem('info-terminal', JSON.stringify(terminal));
    sessionStorage.setItem('refresh-token', RefreshToken);
    getGlobalConfig({login:true})

    if (PrimerLogin) {
      history.push("/confirmPassword");
    } else {
      history.push("/");
    }

  }

  const handleForm=(e)=>{
    let {name,value}=e.target;
    setForm({
      ...Form,
      [name]:{...Form[name],value}
    })
  }
    return (
      <div className="d-flex align-items-stretch w-100 h-100" id="pageLogin">
        <section className="w-100 bg-ppal">

        </section>
        <section className="w-100 d-flex align-items-center justify-content-center">
          {
            (AccesosMultiple)?
            <section className="wrapTerminal">
              <figure>
                <img src={logoCli}/>
              </figure>
              <h2 className="cTexto1 f20 text-center">{t('seleccionarTerm')}</h2>
              <div className="listaTerminales d-flex flex-wrap">
                {
                  Terminales.map((t)=>{
                    return <div onClick={()=>handleTerminal(t)} key={t.Id} style={{opacity:!horarioValido(t,global.Timezone)?.5:1}}>
                      <i className={`icon-reloj-de-pared ${!horarioValido(t,global.Timezone)?'text-danger':'text-success'}`}></i>
                      <p className='cTexto2'>{t.IdTerminal}</p>
                      <p className='cTexto2'>{t.NombreComercial}</p>
                    </div>
                  })
                }
              </div>
            </section>
            :
            <section className="wrapLogin">
              <figure>
                <img src={logoCli}/>
              </figure>
              <h2 className="cTexto1 f20">{t('bienvenido')}</h2>
              <form onSubmit={submit} autoComplete="new-password">
                <div className="row">
                  <div className="col-12">
                    <div className="wrapInput">
                      <input onChange={handleForm} type="text" value={Form.usuario.value} name="usuario" autoComplete="new-password"/>
                      <label className="placeHolder">{t('formUsuario')}</label>
                      {(Form.usuario.error)&&<span className="errorForm mt-3 d-block">{t('campoRequerido')}</span>}
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="wrapInput">
                      <input onChange={handleForm} type="password" value={Form.password.value} name="password" autoComplete="new-password"/>
                      <label className="placeHolder">{t('formClave')}</label>
                      {(Form.password.error)&&<span className="errorForm mt-3 d-block">{t('campoRequerido')}</span>}
                    </div>
                  </div>
                  <div className="col-12">
                    <Link to='/forgotPass' className="float-right cTexto2">{t('olvideClave')}</Link>
                  </div>
                  <div className="col-12">
                    {
                      Loading?
                      <button className="btn btnGeneral d-flex align-items-center justify-content-center w-100" disabled={true}><span className="w-100"><Spinner/></span></button>
                      :
                      <button className="btn btn-block btnGeneral"><span>{t('buttonLogin')}</span></button>
                    }
                  </div>
                  {(ErrorLogin)&&<div className="col-12">
                    <span className="errorForm mt-3 d-block text-center">{t('errorLogin')}</span>
                  </div>}
                </div>
              </form>
            </section>
          }
        </section>
      </div>
    );
}

export default withTranslation()(Login)