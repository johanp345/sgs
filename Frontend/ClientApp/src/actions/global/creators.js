import {
  startGetConfig,
  setGlobalConfig,
  errorGetConfig,
  setIdioma,
} from '.';

import API from '../../services/services';

export const getGlobalConfig = (domain) => async (dispatch) => {
  try {
    dispatch(startGetConfig());
    const { status, data } = await API.global.getConfigGen();
    if (status === 200) {
      dispatch(setGlobalConfig(data));
    } else {
      dispatch(errorGetConfig('Error in get Config.'));
    }
  } catch (error) {
    console.log(error);
    dispatch(errorGetConfig('Error in get Config.'));
  }
};

export const getIdioma = (idioma) => async (dispatch) => {
  try {
      dispatch(setIdioma(idioma));
  } catch (error) {
      dispatch(errorGetConfig('Error in get idioma.'));
  }
};
