import {
  GET_MENU
} from "./types";


export const setMenu = (value) => ({
  type: GET_MENU,
  payload: value,
});


