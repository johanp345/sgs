import {
  setMenu
} from '.';


export const getMenu = (menu) => async (dispatch) => {
  try {
      dispatch(setMenu(menu));
  } catch (error) {
      console.log(error);
  }
};
